$(document).ready(function () {
   $(".footerOne").hover(function () {
      $(".moreReadBtnCustom").css({
         "display" : "block",
         "margin" : "2px 0 0 0px"
      });
      $(".footerOneText").css("display" , "none");
      $(".footerOneDate").css("display" , "none")
   });
   $(".footerOne").mouseleave(function () {
      $(".moreReadBtnCustom").css("display" , "none");
      $(".footerOneText").css("display" , "block");
      $(".footerOneDate").css("display" , "block")
   });

   /*===========card Two============*/
   $(".footerTwo").hover(function () {
      $(".moreReadBtnCustomTwo").css({
         "display" : "block",
         "margin" : "2px 0 0 0px"
      });
      $(".footerTwoText").css("display" , "none");
      $(".footerTwoDate").css("display" , "none")
   });
   $(".footerTwo").mouseleave(function () {
      $(".moreReadBtnCustomTwo").css("display" , "none");
      $(".footerTwoText").css("display" , "block");
      $(".footerTwoDate").css("display" , "block")
   });
   /*===========card three============*/
   $(".footerThree").hover(function () {
      $(".moreReadBtnCustomThree").css({
         "display" : "block",
         "margin" : "2px 0 0 0px"
      });
      $(".footerThreeText").css("display" , "none");
      $(".footerThreeDate").css("display" , "none")
   });
   $(".footerThree").mouseleave(function () {
      $(".moreReadBtnCustomThree").css("display" , "none");
      $(".footerThreeText").css("display" , "block");
      $(".footerThreeDate").css("display" , "block")
   });
   /*===============card four===============*/
   $(".footerFour").hover(function () {
      $(".moreReadBtnCustomFour").css({
         "display" : "block",
         "margin" : "2px 0 0 0px"
      });
      $(".footerFourText").css("display" , "none");
      $(".footerFourDate").css("display" , "none")
   });
   $(".footerFour").mouseleave(function () {
      $(".moreReadBtnCustomFour").css("display" , "none");
      $(".footerFourText").css("display" , "block");
      $(".footerFourDate").css("display" , "block")
   });
});
